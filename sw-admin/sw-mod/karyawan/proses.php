<?php
session_start();
if (empty($_SESSION['SESSION_USER']) && empty($_SESSION['SESSION_ID'])) {
  header('location:../../login/');
  exit;
} else {
  require_once '../../../sw-library/sw-config.php';
  require_once '../../login/login_session.php';
  require_once '../../../sw-library/sw-function.php';
  require_once '../../../sw-library/qr_code/qrlib.php';

  $max_size = 2000000; //2MB
  $salt = '$%DEf0&TTd#%dSuTyr47542"_-^@#&*!=QxR094{a911}+';

  switch (@$_GET['action']) {

    case 'add':
      $error = array();

      if (empty($_POST['employees_nip'])) {
        $error[] = 'NUPTK tidak boleh kosong';
      } else {
        $employees_nip = anti_injection($_POST['employees_nip']);
      }

      if (empty($_POST['employees_email'])) {
        $error[] = 'Email tidak boleh kosong';
      } else {
        $employees_email = strip_tags($_POST['employees_email']);
      }

      if (empty($_POST['employees_password'])) {
        $error[] = 'Password tidak boleh kosong';
      } else {
        $employees_password = strip_tags(hash('sha256', $salt . $_POST['employees_password']));
      }

      if (empty($_POST['employees_name'])) {
        $error[] = 'Nama tidak boleh kosong';
      } else {
        $employees_name = anti_injection($_POST['employees_name']);
      }

      if (empty($_POST['position_id'])) {
        $error[] = 'Posisi tidak boleh kosong';
      } else {
        $position_id = anti_injection($_POST['position_id']);
      }

      if (empty($_POST['shift_id'])) {
        $error[] = 'Shift Kerja tidak boleh kosong';
      } else {
        $shift_id = anti_injection($_POST['shift_id']);
      }

      if (empty($_POST['building_id'])) {
        $error[] = 'Lokasi tidak boleh kosong';
      } else {
        $building_id = anti_injection($_POST['building_id']);
      }

      if (empty($error)) {
        if (filter_var($employees_email, FILTER_VALIDATE_EMAIL)) {
          $query = "SELECT employees_email from employees where employees_email='$employees_email'";
          $result = $connection->query($query) or die($connection->error . __LINE__);
          if (!$result->num_rows > 0) {
            $add = "INSERT INTO employees (
                employees_nip,
                employees_email,
                employees_password,
                employees_name,
                position_id,
                shift_id,
                building_id) VALUES (
                '$employees_nip',
                '$employees_email',
                '$employees_password',
                '$employees_name',
                '$position_id',
                '$shift_id',
                '$building_id')";
            if ($connection->query($add) === false) {
              echo 'Data tidak berhasil disimpan! Error: ' . $connection->error;
            } else {
              echo 'success';
            }
          } else {
            echo 'Sepertinya Email "' . $employees_email . '" sudah terdaftar!';
          }
        } else {
          echo 'Email yang anda masukkan salah!';
        }
      } else {
        echo 'Bidang inputan masih ada yang kosong!';
      }

      /* ------------------------------
          Update
      ---------------------------------*/
      break;
    case 'update':
      $error = array();
      if (empty($_POST['id'])) {
        $error[] = 'ID tidak boleh kosong';
      } else {
        $id = anti_injection($_POST['id']);
      }

      if (empty($_POST['employees_nip'])) {
        $error[] = 'NUPTK tidak boleh kosong';
      } else {
        $employees_nip = anti_injection($_POST['employees_nip']);
      }


      if (empty($_POST['employees_name'])) {
        $error[] = 'Nama tidak boleh kosong';
      } else {
        $employees_name = anti_injection($_POST['employees_name']);
      }


      if (empty($_POST['position_id'])) {
        $error[] = 'Posisi tidak boleh kosong';
      } else {
        $position_id = anti_injection($_POST['position_id']);
      }

      if (empty($_POST['shift_id'])) {
        $error[] = 'Shift Kerja tidak boleh kosong';
      } else {
        $shift_id = anti_injection($_POST['shift_id']);
      }

      if (empty($_POST['building_id'])) {
        $error[] = 'Lokasi tidak boleh kosong';
      } else {
        $building_id = anti_injection($_POST['building_id']);
      }

      if (empty($error)) {
        $update = "UPDATE employees SET employees_nip='$employees_nip',
            employees_name='$employees_name',
            position_id='$position_id',
            shift_id='$shift_id',
            building_id='$building_id' WHERE id='$id'";
        if ($connection->query($update) === false) {
          die($connection->error . __LINE__);
          echo 'Data tidak berhasil disimpan!';
        } else {
          echo 'success';
        }
      } else {
        foreach ($error as $key => $values) {
          echo $values;
        }
      }

      /* --------------- Update Password ------------*/
      break;
    case 'update-password':

      $error = array();
      if (empty($_POST['id'])) {
        $error[] = 'ID tidak boleh kosong';
      } else {
        $id = anti_injection($_POST['id']);
      }

      if (empty($_POST['employees_email'])) {
        $error[] = 'tidak boleh kosong';
      } else {
        $employees_email = anti_injection($_POST['employees_email']);
      }

      if (empty($_POST['employees_password'])) {
        $error[] = 'tidak boleh kosong';
      } else {
        $employees_password = anti_injection($_POST['employees_password']);
        $password_baru = strip_tags(hash('sha256', $salt . $employees_password));
      }

      if (empty($error)) {

        $query = "SELECT  employees_name,employees_email from employees where id='$id'";
        $result = $connection->query($query);
        if ($result->num_rows > 0) {
          $row = $result->fetch_assoc();

          $update = "UPDATE employees SET employees_password='$password_baru' WHERE id='$id'";
          if ($connection->query($update) === false) {
            die($connection->error . __LINE__);
            echo 'Data tidak berhasil disimpan!';
          } else {
            echo 'success';
          }
        } else {
          echo 'Bidang inputan tidak boleh ada yang kosong..!';
        }
      }

      break;

    /* --------------- Delete ------------*/
    case 'delete':
      $id = anti_injection(epm_decode($_POST['id']));

      $cari = mysqli_query($connection, "SELECT photo,employees_code from employees WHERE id='$id'");
      $data = mysqli_fetch_assoc($cari);
      $images_delete = strip_tags($data['photo']);
      $directory = '../../../sw-content/karyawan/' . $images_delete . '';

      $deleted = "DELETE FROM employees WHERE id='$id'";
      if ($connection->query($deleted) === true) {
        echo 'success';

      } else {
        //tidak berhasil
        echo 'Data tidak berhasil dihapus.!';
        die($connection->error . __LINE__);
      }

      break;

    /* --------------- Clear Device ------------*/
    case 'clear':
      $id = anti_injection(epm_decode($_POST['id']));
      $cari = mysqli_query($connection, "SELECT last_login_device from employees WHERE id='$id'");
      $data = mysqli_fetch_assoc($cari);

      $update = "UPDATE employees
  SET last_login_device = NULL";
      if ($connection->query($update) === true) {
        echo 'success';
      } else {
        //tidak berhasil
        echo 'Gagal menghapus device terakhir login!';
        die($connection->error . __LINE__);
      }

      break;

  }

}