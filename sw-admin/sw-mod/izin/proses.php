<?php
session_start();
if (empty($_SESSION['SESSION_USER']) && empty($_SESSION['SESSION_ID'])) {
  header('location:../../login/');
  exit;
} else {
  require_once '../../../sw-library/sw-config.php';
  require_once '../../login/login_session.php';
  include('../../../sw-library/sw-function.php');

  switch (@$_GET['action']) {
    case 'delete':
      $id = mysqli_real_escape_string($connection, epm_decode($_POST['id']));
      $employees_id = mysqli_real_escape_string($connection, epm_decode($_POST['employees_id']));
      $query_delete = "SELECT files,permission_date,permission_date_finish from permission WHERE employees_id='$employees_id' AND permission_id='$id'";
      $result_delete = $connection->query($query_delete);
      if ($result_delete->num_rows > 0) {
        $row = $result_delete->fetch_assoc();

        $start = date('Y-m-d', strtotime('-1 days', strtotime($row['permission_date'])));
        $finish = date('Y-m-d', strtotime('-1 days', strtotime($row['permission_date_finish'])));

        $images_delete = strip_tags($row['files']);
        $directory = '../../../sw-content/izin/' . $images_delete . '';
        if (file_exists("../../../sw-content/izin/$images_delete")) {
          unlink($directory);
        }

        while ($start <= $finish) {
          $start = date('Y-m-d', strtotime('+1 days', strtotime($start)));
          $deleted_absent = "DELETE FROM presence WHERE employees_id='$employees_id' AND presence_date='$start'";
          $connection->query($deleted_absent);
        }
        $deleted = "DELETE FROM permission WHERE  permission_id='$id'";
        if ($connection->query($deleted) === true) {
          echo 'success';
        } else {
          //tidak berhasil
          echo 'Data tidak berhasil dihapus.!';
          die($connection->error . __LINE__);
        }
      }

      break;

    case 'terima':
      $id = mysqli_real_escape_string($connection, epm_decode($_POST['id']));
      $employees_id = mysqli_real_escape_string($connection, epm_decode($_POST['employees_id']));

      $query_status = "SELECT status, permission_date, permission_date_finish 
                    FROM permission
                    WHERE employees_id='$employees_id'
                    AND permission_id='$id'";
      $result_status = $connection->query($query_status);
      $newStatus = $result_status->fetch_assoc();

      if ($result_status->num_rows > 0) {
        $update_status = "2";
        $update_description = "Permohonan izin ini telah direview";

        $start = date('Y-m-d', strtotime('-1 days', strtotime($newStatus['permission_date'])));
        $finish = date('Y-m-d', strtotime('-1 days', strtotime($newStatus['permission_date_finish'])));

        $type = "SELECT type
              FROM permission
              WHERE employees_id='$employees_id'
              AND permission_id='$id'";
        $result_type = $connection->query($type);
        $newType = $result_type->fetch_assoc();

        if ($newType['type'] == 'Izin') {
          $presentId = '3';
          $postType = $newType['type'];
        } else {
          $presentId = '2';
          $postType = $newType['type'];
        }

        while ($start <= $finish) {
          $start = date('Y-m-d', strtotime('+1 days', strtotime($start)));
          $add_absent = "INSERT INTO presence (employees_id,
                          presence_date,
                          time_in,
                          time_out,
                          present_id,
                          latitude_longtitude_in,
                          latitude_longtitude_out,
                          information) VALUES ('$employees_id',
                          '$start',
                          '00.00.00',
                          '00:00:00',
                          '$presentId', /*present_id*/
                          '',
                          '',
                          '$postType')";
          $result_absent = $connection->query($add_absent);
        }

        $update_query = mysqli_query($connection, "UPDATE permission SET status = '$update_status', permission_description = '$update_description' WHERE permission_id = '$id' AND employees_id = '$employees_id'");
        if ($result_absent === true && $update_query === true) {
          echo 'success';
        } else {
          // tidak berhasil
          echo 'Data permohonan izin tidak berhasil diterima!';
          die($connection->error . __LINE__);
        }
      }
      break;

    case 'tolak':
      $id = mysqli_real_escape_string($connection, epm_decode($_POST['id']));
      $employees_id = mysqli_real_escape_string($connection, epm_decode($_POST['employees_id']));

      $query_status = "SELECT status, permission_description
                 FROM permission
                 WHERE employees_id = '$employees_id'
                 AND permission_id = '$id'";
      $result_status = $connection->query($query_status);

      if ($result_status->num_rows > 0) {
        $update_status = "UPDATE permission
                      SET status = '3', permission_description = 'Permohonan izin ini telah direview'
                      WHERE permission_id = '$id'
                      AND employees_id = '$employees_id'";
        $result_update = mysqli_query($connection, $update_status);

        if ($result_update === true) {
          echo 'success';
        } else {
          // tidak berhasil
          echo 'Data permohonan izin tidak berhasil ditolak!';
          die($connection->error . __LINE__);
        }
      }

      break;

  }

}