<?php
require_once'../sw-library/sw-config.php';

// Mengambil data profil pengguna berdasarkan email
if (isset($_GET['email'])) {
    $email = $_GET['email'];
    // $email = 'acengruswa158@gmail.com';

    $query = "SELECT employees.id, employees.employees_nip, employees.employees_email, employees.employees_name, position.position_name, shift.time_in, shift.time_out, building.name
              FROM employees
              INNER JOIN position ON employees.position_id = position.position_id
              INNER JOIN shift ON employees.shift_id = shift.shift_id
              INNER JOIN building ON employees.building_id = building.building_id
              WHERE employees.employees_email = '$email'";
    $result = mysqli_query($connection, $query);

    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        $response = array(
            'id' => $row['id'],
            'employees_nip' => $row['employees_nip'],
            'employees_email' => $row['employees_email'],
            'employees_name' => $row['employees_name'],
            'position_name' => $row['position_name'],
            'time_in' => $row['time_in'],
            'time_out' => $row['time_out'],
            "building_name" => $row['name'],
        );

        // Mengirimkan response sebagai JSON
        header('Content-Type: application/json');
        echo json_encode($response);
    } else {
        http_response_code(404);
        $response = array('error' => 'Data pengguna tidak ditemukan');
        header('Content-Type: application/json');
        echo json_encode($response);
    }
} else {
    http_response_code(400);
    $response = array('error' => 'Email pengguna tidak diberikan');
    header('Content-Type: application/json');
    echo json_encode($response);
}

// Menutup koneksi database
mysqli_close($connection);
?>
