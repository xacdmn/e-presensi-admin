<?php
date_default_timezone_set('Asia/Jakarta');
require_once '../sw-library/sw-config.php';

// Mengambil data employees_id dan bulan-tahun dari parameter GET
$employees_id = $_GET['employees_id'];
$month = $_GET['month'];
$year = $_GET['year'];

// $employees_id = '30';
// $month = '7';
// $year = '2023';

// Mengatur zona waktu menjadi Waktu Indonesia Barat (WIB)

$query_employees = "SELECT shift_id
                    FROM employees
                    WHERE id = '$employees_id'";
$result_employees = $connection->query($query_employees);
$row_employees = $result_employees->fetch_assoc();
$shift_id = $row_employees['shift_id'];

$query_shift = "SELECT time_in
                FROM shift
                WHERE shift_id = '$shift_id'";
$result_shift = $connection->query($query_shift);
$row_shift = $result_shift->fetch_assoc();
$shift_time_in = $row_shift['time_in'];

// Mendapatkan tanggal awal dan akhir bulan yang diminta
$startDate = date('Y-m-01', strtotime("$year-$month-01"));
$endDate = date('Y-m-t', strtotime("$year-$month-01"));

// Mengambil histori presensi dalam rentang tanggal yang diminta
$sql = "SELECT present_status.present_name, presence.presence_date, presence.time_in, presence.time_out
        FROM presence
        INNER JOIN present_status ON presence.present_id = present_status.present_id
        WHERE presence.employees_id = '$employees_id'
        AND presence.presence_date >= '$startDate'
        AND presence.presence_date <= '$endDate'";
$result = $connection->query($sql);

// Membangun array respon JSON
$response = array();
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $entry = array();
        $entry['present_name'] = $row['present_name'];
        $entry['date'] = $row['presence_date'];
        if ($row['time_in'] > $shift_time_in) {
            $entry['time_in'] = $row['time_in'];
            $entry['status'] = 'Terlambat';
        } else {
            $entry['time_in'] = $row['time_in'];
            $entry['status'] = 'Tepat Waktu';
        }
        $entry['time_out'] = $row['time_out'];
        $response[] = $entry;
    }
}

// Generate responses for missing days
$startTimestamp = strtotime($startDate);
$endTimestamp = strtotime($endDate);
$currentTimestamp = $startTimestamp;

while ($currentTimestamp <= $endTimestamp) {
    $currentDate = date('Y-m-d', $currentTimestamp);
    $found = false;
    
    foreach ($response as $entry) {
        if ($entry['date'] === $currentDate) {
            $found = true;
            break;
        }
    }
    
    if (!$found) {
        $zeroTime = date('H:i:s', strtotime('00:00:00'));
        $entry = array(
            'present_name' => 'Tidak Hadir',
            'date' => $currentDate,
            'time_in' => $zeroTime,
            'time_out' => $zeroTime,
            'status' => 'Tidak Hadir'
        );
        $response[] = $entry;
    }
    
    $currentTimestamp = strtotime('+1 day', $currentTimestamp);
}

// Sort the response array by date
usort($response, function ($a, $b) {
    return strtotime($a['date']) - strtotime($b['date']);
});

// Mengirim respon JSON
header('Content-Type: application/json');
echo json_encode($response);

// Menutup koneksi database
$connection->close();
?>
