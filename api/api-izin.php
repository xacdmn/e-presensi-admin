<?php
date_default_timezone_set('Asia/Jakarta');
require_once '../sw-library/sw-config.php';

// Mengambil data employees_id dan bulan-tahun dari parameter GET
$employees_id = $_GET['employees_id'];
$month = $_GET['month'];
$year = $_GET['year'];

// $employees_id = '30';
// $month = '6';
// $year = '2023';

// Mendapatkan tanggal awal dan akhir bulan yang diminta
$startDate = date('Y-m-01', strtotime("$year-$month-01"));
$endDate = date('Y-m-t', strtotime("$year-$month-01"));

// Membuat query untuk mendapatkan izin berdasarkan employees_id dan bulan
$query = "SELECT p.permission_id, p.permission_name, p.permission_date, p.permission_date_finish, p.permission_description, p.type, p.date, s.status_name 
            FROM permission p
            INNER JOIN status s ON p.status = s.status_id
            WHERE p.employees_id = '$employees_id'
            AND date >= '$startDate'
            AND date <= '$endDate'";
$result = $connection->query($query);

// Membangun array respon JSON
$response = array();
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $entry = array();
        $entry['permission_name'] = $row['permission_name'];
        $entry['date'] = $row['date'];
        $entry['start_date'] = $row['permission_date'];
        $entry['finish_date'] = $row['permission_date_finish'];
        $entry['description'] = $row['permission_description'];
        $entry['type'] = $row['type'];
        $entry['status'] = $row['status'];
        $entry['status_name'] = $row['status_name'];
        $response[] = $entry;
    }
}

// Mengecek apakah ada data yang ditemukan
if (empty($response)) {
    // Data tidak ditemukan, mengatur response status code ke 404 Not Found
    http_response_code(404);
}

// Mengirim respon JSON
header('Content-Type: application/json');
echo json_encode($response);

// Menutup koneksi database
$connection->close();
?>
