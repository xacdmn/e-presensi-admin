<?php
date_default_timezone_set('Asia/Jakarta');
require_once '../sw-library/sw-config.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Pastikan parameter yang diperlukan ada
    if (
        isset($_POST['employees_id']) && 
        isset($_POST['permission_name']) && 
        isset($_POST['permission_date']) && 
        isset($_POST['permission_date_finish']) && 
        isset($_POST['type']) && 
        isset($_FILES['file'])
    ) {
        $employeesId = $_POST['employees_id'];
        $permissionName = $_POST['permission_name'];
        $permissionDate = $_POST['permission_date'];
        $permissionDateFinish = $_POST['permission_date_finish'];
        $type = $_POST['type'];

        // Check if the data already exists
        $query = "SELECT files 
                    FROM permission
                    WHERE employees_id = '$employeesId'
                    AND permission_date BETWEEN '$permissionDate' AND '$permissionDateFinish'
                    OR permission_date_finish BETWEEN '$permissionDate' AND '$permissionDateFinish'";      
        $result = $connection->query($query);
        
        if ($result->num_rows == 0) {
            $file = $_FILES['file'];
            $fileName = $file['name'];
            $fileTmpName = $file['tmp_name'];
            $uniqueString = uniqid();
        
            // Generate nama file baru dengan menambahkan tanggal saat ini (date now) dan id karyawan (employees id)
            $fileExtension = pathinfo($fileName, PATHINFO_EXTENSION);
            $newFileName = date('YmdHis') . '_' . $employeesId . '_' . $uniqueString . $fileName;
        
            // Tentukan direktori penyimpanan file
            $uploadDirectory = '../sw-content/izin/'; // Ubah direktori penyimpanan file

            // Pindahkan file ke direktori penyimpanan dengan nama baru
            if (move_uploaded_file($fileTmpName, $uploadDirectory . $newFileName)) {
                $sql = "INSERT INTO `permission` (`employees_id`, `permission_name`, `permission_date`, `permission_date_finish`, `permission_description`, `files`, `type`, `date`, `status`) 
                VALUES ('$employeesId','$permissionName','$permissionDate','$permissionDateFinish', 'Permohonan izin ini perlu direview', '$newFileName','$type',NOW(),'1')";
                if ($connection->query($sql) === TRUE) {
                    $response = array(
                        'status' => true,
                        'message' => "Pengajuan izin berhasil dikirim"
                    );
                } else {
                    $response = array(
                        'status' => false,
                        'message' => "Gagal menambahkan pengajuan data data"
                    );
                }
            echo json_encode($response);
            } else {
            // Jika gagal memindahkan file, berikan respons error
            $response = array(
                'status' => false,
                'message' => 'Terjadi kesalahan saat mengunggah file.'
            );
            
            echo json_encode($response);
            }
        } else {

            $response = array(
                'status' => false,
                'message' => 'Sebelumnya data sudah ada pada tanggal ' .date('d-m-Y', strtotime($permissionDate)) . ' atau ' . date('d-m-Y', strtotime($permissionDateFinish))
            );
            
            echo json_encode($response);
        }
    } else {
        // Jika parameter yang diperlukan tidak ada, berikan respons error
        $response = array(
            'status' => false,
            'message' => 'Data yang anda masukan tidak lengkap.'
        );
        
        echo json_encode($response);
    }
} else {
    // Jika bukan metode POST, berikan respons error
    $response = array(
        'status' => false,
        'message' => 'Metode HTTP tidak valid.'
    );
    
    echo json_encode($response);
}
?>
