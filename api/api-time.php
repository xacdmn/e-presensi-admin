<?php
require_once '../sw-library/sw-config.php';

// Mendapatkan nilai parameters dari permintaan API
$employees_id = $_GET['employees_id'];
$date = date("Y-m-d");

$queryGetTime = "SELECT time_in, time_out
FROM presence
WHERE employees_id = '$employees_id' AND presence_date = '$date'";
$result_time = $connection->query($queryGetTime);
$row_presence = $result_time->fetch_assoc();
$time_in = $row_presence['time_in'];
$time_out = $row_presence['time_out'];

$response = array(
    'time_in' => isset($time_in) ? $time_in : '00:00:00',
    'time_out' => isset($time_out) ? $time_out : '00:00:00',
);

// Return the JSON response
header('Content-Type: application/json');
echo json_encode($response);

// Menutup koneksi ke database
$connection->close();
?>