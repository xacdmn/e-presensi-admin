<?php
require_once '../sw-library/sw-config.php';

$salt = '$%DEf0&TTd#%dSuTyr47542"_-^@#&*!=QxR094{a911}+';

// Retrieve POST data
$email = $_POST['email'];
$password = hash('sha256', $salt . $_POST['password']);
$deviceID = $_POST['deviceID'];

// Validate credentials
$sql = "SELECT * FROM employees WHERE employees_email = '$email' AND employees_password = '$password'";
$result = $connection->query($sql);

if ($result->num_rows > 0) {
    $row = mysqli_fetch_assoc($result);
    $lastLoginDevice = $row['last_login_device'];

    if (empty($lastLoginDevice)) {
        // If last login device is empty, update it with the current device ID
        $updateSql = "UPDATE employees SET last_login_device = '$deviceID' WHERE employees_email = '$email'";
        $connection->query($updateSql);
    } else if ($lastLoginDevice !== $deviceID) {
        // If last login device is not empty and doesn't match the current device ID, provide a false response
        $data = array('success' => 'invalid device', 'message' => 'perangkat tidak valid');
        http_response_code(200);
        $connection->close();
        header('Content-Type: application/json');
        echo json_encode($data);
        exit();
    }

    $data = array(
        'success' => 'true',
        'message' => 'Login berhasil',
        'id' => $row['id'],
        'employees_code' => $row['employees_code'],
        'employees_nip' => $row['employees_nip'],
        'employees_email' => $row['employees_email'],
        'employees_name' => $row['employees_name'],
    );

    // Login successful
    http_response_code(200);
} else {
    // Login failed
    $data = array('success' => 'false', 'message' => 'Email atau kata sandi salah');
    http_response_code(200);
}

// Close database connection
$connection->close();

// Set the response headers and output the JSON response
header('Content-Type: application/json');
echo json_encode($data);

?>
