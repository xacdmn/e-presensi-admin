<?php
require_once '../sw-library/sw-config.php';

// Mendapatkan nilai parameters dari permintaan API
$selectedMonth = $_GET["selectedMonth"];
$employeesId = $_GET['employeesId'];
$selectedYears = $_GET['selectedYears'];

// Membuat dan menjalankan query
$query_employees = "SELECT shift_id
                      FROM employees
                      WHERE id = '$employeesId'";
$result_employees = $connection->query($query_employees);
$row_employees = $result_employees->fetch_assoc();
$shift_id = $row_employees['shift_id'];

$query_shift = "SELECT time_in
                FROM shift
                WHERE shift_id = '$shift_id'";
$result_shift = $connection->query($query_shift);
$row_shift = $result_shift->fetch_assoc();
$shift_time_in = $row_shift['time_in'];

$query_hadir = "SELECT COUNT(presence_id) AS total_kehadiran
                FROM presence 
                WHERE employees_id = '$employeesId' 
                AND MONTH(presence_date) = '$selectedMonth' 
                AND YEAR(presence_date) = '$selectedYears'
                AND present_id = '1'";
$result_hadir = $connection->query($query_hadir);
$row_hadir = $result_hadir->fetch_assoc();
$total_kehadiran = $row_hadir['total_kehadiran'];

$query_sakit = "SELECT COUNT(presence_id) AS total_sakit
                FROM presence 
                WHERE employees_id = '$employeesId' 
                AND MONTH(presence_date) = '$selectedMonth' 
                AND YEAR(presence_date) = '$selectedYears'
                AND present_id = '2'";
$result_hadir = $connection->query($query_sakit);
$row_hadir = $result_hadir->fetch_assoc();
$total_sakit = $row_hadir['total_sakit'];

$query_izin = "SELECT COUNT(presence_id) AS total_izin
                FROM presence 
                WHERE employees_id = '$employeesId' 
                AND MONTH(presence_date) = '$selectedMonth' 
                AND YEAR(presence_date) = '$selectedYears'
                AND present_id = '3'";
$result_hadir = $connection->query($query_izin);
$row_hadir = $result_hadir->fetch_assoc();
$total_izin = $row_hadir['total_izin'];

$query_terlambat = "SELECT COUNT(presence_id) AS total_terlambat
                    FROM presence
                    WHERE employees_id = '$employeesId' 
                    AND MONTH(presence_date) = '$selectedMonth' 
                    AND YEAR(presence_date) = '$selectedYears'
                    AND TIME(time_in) > TIME('$shift_time_in')";
$result_terlambat = $connection->query($query_terlambat);
$row_terlambat = $result_terlambat->fetch_assoc();
$total_terlambat = $row_terlambat['total_terlambat'];

// Mengirim hasil sebagai respon JSON
$response = array(
    'total_kehadiran' => isset($total_kehadiran) ? $total_kehadiran : 0,
    'total_terlambat' => isset($total_terlambat) ? $total_terlambat : 0,
    'total_sakit' => isset($total_sakit) ? $total_sakit : 0,
    'total_izin' => isset($total_izin) ? $total_izin : 0
);

header('Content-Type: application/json');
echo json_encode($response);

// Menutup koneksi ke database
$connection->close();
?>
