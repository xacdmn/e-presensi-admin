<?php
date_default_timezone_set('Asia/Jakarta');
require_once '../sw-library/sw-config.php';
// Fungsi untuk menghitung jarak antara dua koordinat menggunakan rumus Haversine
function calculateDistance($latitude1, $longitude1, $latitude2, $longitude2)
{
    // Mengubah derajat menjadi radian
    $lat1 = deg2rad($latitude1);
    $lon1 = deg2rad($longitude1);
    $lat2 = deg2rad($latitude2);
    $lon2 = deg2rad($longitude2);

    // Radius bumi dalam meter
    $earthRadius = 6378140;

    // Menghitung selisih latitude dan longitude
    $deltaLat = $lat2 - $lat1;
    $deltaLon = $lon2 - $lon1;

    // Menghitung jarak menggunakan rumus Haversine
    $a = sin($deltaLat / 2) * sin($deltaLat / 2) + cos($lat1) * cos($lat2) * sin($deltaLon / 2) * sin($deltaLon / 2);
    $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
    $distance = $earthRadius * $c;

    // Mengembalikan jarak dalam meter
    return $distance;
}

// // Get the QR code value from the request body
$qrCode = $_POST['qr_code'];

// // Get other required data from the request query parameters
$employees_id = $_GET['employees_id'];
// $employees_id = 30;
$latitude1 = $_GET['latitude'];
$longitude1 = $_GET['longitude'];
$date = $_GET['date'];
$time = $_GET['time'];
$isPresensi = $_GET['presensi'];

if (!empty($employees_id)) {

    $queryGetUniqeCode = "SELECT building.code, building.latitude_longtitude, building.radius
    FROM building
    INNER JOIN employees ON building.building_id = employees.building_id
    WHERE employees.id = $employees_id";
    $result_code = $connection->query($queryGetUniqeCode);
    $row_building = $result_code->fetch_assoc();
    $validQRCODE = $row_building['code'];

    $location = explode(',', $row_building['latitude_longtitude']);
    $latitude2 = $location[0];
    $longitude2 = $location[1];

    $radius = $row_building['radius'];

    $presentId = 1;

    if ($qrCode === $validQRCODE) {
        if ($latitude1 && $longitude1 && $latitude2 && $longitude2) {
            $distance = calculateDistance($latitude1, $longitude1, $latitude2, $longitude2);

            if ($distance <= $radius) {
                if ($isPresensi == "Masuk") {
                    $add_absent = "INSERT INTO presence (employees_id,
                    presence_date,
                    time_in,
                    time_out,
                    present_id,
                    latitude_longtitude_in,
                    latitude_longtitude_out,
                    information) VALUES ('$employees_id',
                    '$date',
                    '$time',
                    '',
                    '$presentId',
                    '$latitude1,$longitude1',
                    '',
                    'Hadir')";
                    $result_absent = $connection->query($add_absent);
                    if ($result_absent === true) {
                        $response = array(
                            'status' => 'success',
                            'message' => 'Anda telah berhasil melakukan presensi hari ini'
                        );
                    } else {
                        http_response_code(400);
                        $response = array(
                            'status' => 'error',
                            'message' => 'Anda gagal melakukan presensi hari ini silahkan coba lagi!'
                        );
                    }
                } else {
                    $update_absent = "UPDATE presence SET
                    time_out = '$time',
                    latitude_longtitude_out = '$latitude1,$longitude1'
                    WHERE employees_id = '$employees_id'
                    AND presence_date = '$date'";
                    $result_update = $connection->query($update_absent);
                    if ($result_update === true) {
                        $response = array(
                            'status' => 'success',
                            'message' => 'Anda telah berhasil melakukan presensi hari ini'
                        );
                    } else {
                        http_response_code(400);
                        $response = array(
                            'status' => 'error',
                            'message' => 'Anda gagal melakukan presensi hari ini silahkan coba lagi!'
                        );
                    }
                }

            } else {
                http_response_code(400);
                $response = array(
                    'status' => 'error',
                    'message' => 'Jarak berada di luar radius yang ditentukan'
                );
            }
        } else {
            http_response_code(404);
            $response = array(
                'status' => 'error',
                'message' => 'Lokasi anda tidak terdeteksi'
            );
        }
    } else {
        http_response_code(400);
        $response = array(
            'status' => 'error',
            'message' => 'QR Code tidak valid! '
        );
    }
} else {
    http_response_code(404);
    $response = array(
        'status' => 'error',
        'message' => 'Akun tidak ditemukan!'
    );
}

// Return the JSON response
header('Content-Type: application/json');
echo json_encode($response);
// Menutup koneksi ke database
$connection->close();
?>